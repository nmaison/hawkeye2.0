import React, { Component } from 'react'
import Image from 'next/image'

export class OLD extends Component {
  render() {
    return (
      <>
        <nav className="navbar-static-side  h-100" style={{backgroundColor: "#1c1c1c"}} role="navigation">
            <ul className="nav metismenu " id="side-menu">
              <li className="nav-header">
                <img
                  id="psLogo"
                  alt="Platform Science"
                  className="img"
                  src="./images/sidebar-logo.svg"
                />
              </li>

              <li>
                <ul className="nav ">
                  <li className="">
                    <a href="/hawkeye">Hawkeye</a>
                  </li>
                  <li className="toggler admin">
                    <a>
                      <span>CVD Deployment</span>
                    </a>
                    <ul className="nav nav-second-level dropdown-content">
                      <li>
                        <a href="/deployments">All Deployments</a>
                      </li>
                      <li>
                        <a href="/schedule">Schedule Deployment</a>
                      </li>
                      <li>
                        <a href="/deploymentsoverview">
                          Deployment Profiles
                          <li>
                            <a href="/deploymentDetails">Deployment Details</a>
                          </li>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="toggler">
                    <a>
                      <span>Proactive Monitoring</span>
                    </a>
                    <ul className="nav nav-second-level dropdown-content">
                      <li>
                        <a href="/esnhistory">History</a>
                      </li>
                      <li>
                        <a href="/support">Support</a>
                      </li>
                      <li>
                        <a href="/deployment">Bulk CVD Actions</a>
                      </li>
                      <li>
                        <a href="/smspage">Sms Sender</a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="/releasemonitor">Release Monitor</a>
                  </li>
                  <li className="toggler admin">
                    <a>
                      <span>Admin Menu</span>
                    </a>
                    <ul className="nav nav-second-level dropdown-content">
                      <li>
                        <a href="/admin">Admin Tools</a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="/logout">Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
        </nav>
      </>
    )
  }
}

export class Navbar extends Component {

   openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";  }
  
   closeNav(){
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";  }
  render() {
    return (<>
<div id="mySidenav" className="sidenav">
  <a href="javascript:void(0)" className="closebtn" onClick={this.closeNav}>&times;</a>
  <ul className="nav metismenu " id="side-menu">
    <div  style={{
          display: "flex",
          justifyContent: "center",
        }}>
       <Image
        src="/sidebar-logo.svg"
        alt="Platform Science"
        layout='fixed'
        width={100}
        height={100}
      />
    </div>

    <li>
      <ul className="nav ">
        <li className="">
          <a href="/hawkeye">Hawkeye</a>
        </li>
        <li className="toggler admin">
          <a>
            <span>CVD Deployment</span>
          </a>
          <ul className="nav nav-second-level dropdown-content">
            <li>
              <a href="/deployments">All Deployments</a>
            </li>
            <li>
              <a href="/schedule">Schedule Deployment</a>
            </li>
            <li>
              <a href="/deploymentsoverview">
                Deployment Profiles
                <li>
                  <a href="/deploymentDetails">Deployment Details</a>
                </li>
              </a>
            </li>
          </ul>
        </li>
        <li className="toggler">
          <a>
            <span>Proactive Monitoring</span>
          </a>
          <ul className="nav nav-second-level dropdown-content">
            <li>
              <a href="/esnhistory">History</a>
            </li>
            <li>
              <a href="/support">Support</a>
            </li>
            <li>
              <a href="/deployment">Bulk CVD Actions</a>
            </li>
            <li>
              <a href="/smspage">Sms Sender</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="/releasemonitor">Release Monitor</a>
        </li>
        <li className="toggler admin">
          <a>
            <span>Admin Menu</span>
          </a>
          <ul className="nav nav-second-level dropdown-content">
            <li>
              <a href="/admin">Admin Tools</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="/logout">Logout</a>
        </li>
      </ul>
    </li>
  </ul>
</div>

<span style={{fontSize:30,cursor:"pointer"}} onClick={this.openNav}>&#9776;</span>

   </>);
  }
}
