import React, { Component } from 'react'

export class DriverBox extends Component {
  render() {
    return (
      <>
        <div className="col-lg-6">
          <div className="ibox float-e-margins">
            <div className="ibox-content first-boxes">
              <div className="border-bottom">
                <h1 className="font-bold">
                  Driver
                  <small
                    id="dutyStatus"
                    className=" btn-lg bg-muted p-xxs text-white pull-right"
                  >
                    DutyStatus
                  </small>
                </h1>
              </div>
              <h4 className="font-bold ">
                Driver Id{' '}
                <small
                  id="driverId"
                  className="pull-right dynamic-data font-bold"
                >
                  {' '}
                </small>
              </h4>
              <h4 className=" font-bold ">
                Last HOS Location
                <small
                  id="timezone"
                  className="pull-right dynamic-data font-bold"
                ></small>
              </h4>
              <h4 className=" font-bold ">
                Last HOS event
                <small
                  id="lastHosEvent"
                  className=" pull-right dynamic-data font-bold"
                ></small>
              </h4>
              <hr />

              <div className="">
                <th className=" font-bold ">Health Check: </th>

                <table className="table table-borderless">
                  <tbody>
                    <tr>
                      <td className=" col-md-3">
                        <div
                          id="diagnosticStatusBox"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          Data Diagnostics Status
                          <p
                            id="diagnosticStatus"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                      <td className="col-md-3">
                        <div
                          id="malfunctionStatusBox"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          Malfunctioning status
                          <p
                            id="malfunctionStatus"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div className="ibox">
                <h4>Driver Actions</h4>
                <a
                  id="driversLogsPage"
                  target="_blank"
                  className=" btn btn-success "
                >
                  View Drivers Logs
                </a>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }
}
