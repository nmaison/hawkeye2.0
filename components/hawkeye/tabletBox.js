import React, { Component } from 'react'

export class TabletBox extends Component {
  render() {
    return (
      <>
        <div className=" col-lg-6 ">
          <div className=" ibox float-e-margins ">
            <div id="tabletBox" className=" ibox-content first-boxes ">
              <div className=" border-bottom ">
                <h1 className=" font-bold ">Tablet</h1>
              </div>
              <h4 className=" font-bold ">
                Serial{' '}
                <small
                  id="displayType"
                  className=" pull-right dynamic-data font-bold"
                ></small>
              </h4>

              <h4 className=" font-bold ">
                Last Event Sent
                <small
                  id="displayLastEvent"
                  className=" pull-right dynamic-data font-bold"
                ></small>
              </h4>
              <h4 className=" font-bold ">
                Environment
                <small
                  id="displayEnvironment"
                  className="pull-right dynamic-data font-bold"
                >
                  {' '}
                </small>
              </h4>
              <h4 className=" font-bold ">
                Android Version
                <small
                  id="displayConfigGroup"
                  className="pull-right dynamic-data font-bold"
                >
                  {' '}
                </small>
              </h4>
            <hr />
            <div>
              <th className=" font-bold ">Health Check: </th>

              <table className=" table table-borderless ">
                <tbody>
                  <tr>
                    <td scope=" row " className=" col-md-3 ">
                      <div
                        id="displayPowerLevelBox"
                        className=" bg-muted p-xxs b-r-md tiny-font modal-details"
                      >
                        Power Level
                        <p
                          id="displayPowerLevel"
                          className="dynamic-data font-bold"
                        >
                          n/a
                        </p>
                      </div>
                    </td>
                    <td scope="row " className="col-md-3 ">
                      <div
                        id="displayConnectionStatusBox"
                        className="bg-muted p-xxs b-r-md tiny-font modal-details"
                      >
                        Connection Status
                        <p
                          id="displayConnectionStatus"
                          className="dynamic-data font-bold"
                        >
                          n/a
                        </p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <h4>Display Actions</h4>
            <a target="_blank" id="unlockTablet" className="btn btn-success ">
              Unlock
            </a>
            <a
              target="_blank"
              id="displayTabletSoftware"
              className="btn btn-success "
            >
              Tablet App Software
            </a>
          </div>
        </div>
        </div>
      </>
    )
  }
}
