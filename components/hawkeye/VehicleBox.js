import React, { Component } from 'react'

export class VehicleBox extends Component {
  render() {
    return (
      <>
        <div className="col-lg-6 ">
          <div className="ibox float-e-margins ">
            <div className="ibox-content first-boxes ">
              <div className="border-bottom ">
                <h1 className="font-bold ">Vehicle</h1>
              </div>
              <h4 className="font-bold ">
                Asset ID{' '}
                <small
                  id="assetId"
                  className="pull-right dynamic-data font-bold"
                >
                  n/a
                </small>
              </h4>

              <h4 className=" font-bold ">
                VIN
                <small id="vin" className="pull-right dynamic-data font-bold">
                  n/a
                </small>
              </h4>
              <h4 className=" font-bold ">
                Make Model
                <small
                  id="decodedVin"
                  className="pull-right dynamic-data font-bold"
                >
                  n/a
                </small>
              </h4>
              <h4 className=" font-bold ">
                Location
                <small
                  id="vehicleLocation"
                  className=" pull-right dynamic-data font-bold"
                ></small>
              </h4>
              <hr />
              <div>
                <th className=" font-bold ">Health Check: </th>

                <table className="table table-borderless ">
                  <tbody>
                    <tr>
                      <td scope="row " className="col-md-3 ">
                        <div
                          id="engineStatusBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className=" bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          Engine Status
                          <p
                            id="engineStatus"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                      <td scope="row " className="col-md-3 ">
                        <div
                          id="odometerStatusBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          Odometer
                          <p
                            id="odometerStatus"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                      <td scope="row " className="col-md-3 ">
                        <div
                          id="fuelStatusBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className=" bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          Fuel Readings
                          <p id="fuelStatus" className="dynamic-data font-bold">
                            n/a
                          </p>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td scope="row ">
                        <div
                          id="jbusErrorsStatusBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          Jbus Errors
                          <p
                            id="jbusErrorsStatus"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>

                      <td scope="row "></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }
}
