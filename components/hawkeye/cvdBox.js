import React, { Component } from 'react'

export class CvdBox extends Component {
  render() {
    return (
      <>
      <div className=" col-lg-6 ">

        <div className=" ibox float-e-margins ">
          <div className=" ibox-content first-boxes">
            <div className=" border-bottom ">
              <h1 className=" font-bold ">
                CVD
                <li id="userIcon" className="fa fa-user pull-right "></li>
                <li id="tabletIcon" className="fa fa-tablet pull-right "></li>
                <li id="truckIcon" className="fa fa-truck pull-right "></li>
              </h1>
            </div>
            <h4 className=" font-bold ">
              ESN{' '}
              <small id="cvdEsn" className=" pull-right dynamic-data font-bold">
                {' '}
              </small>
            </h4>

            <h4 className=" font-bold ">
              Last Heartbeat
              <small
                id="lastHeartbeat"
                className=" pull-right dynamic-data font-bold"
              ></small>
            </h4>
            <h4 className=" font-bold ">
              Portal Config:{' '}
              <small className=" pull-right dynamic-data font-bold">
                <a id="cvdConfig" href=""></a>{' '}
              </small>
            </h4>
            <h4 className=" font-bold ">
              Environment
              <small
                id="environment"
                className=" pull-right dynamic-data font-bold"
              >
                {' '}
              </small>
              <p>
                <a id="deviceManagerLink" target="_blank" href="">
                  Device Manager
                </a>
              </p>
            </h4>

            <hr />
              <div>
                <th className=" font-bold ">Health Check: </th>

                <table className="table table-borderless ">
                  <tbody>
                    <tr>
                      <td scope="row " className="col-md-3 ">
                        <div
                          id="softwareHealthCheckBox"
                          className=" bg-muted p-xxs b-r-md tiny-font modal-details"
                          data-toggle="tooltip"
                          data-placement="top"
                        >
                          Software
                          <p
                            id="softwareHealthCheck"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                      <td scope="row " className="col-md-3 ">
                        <div
                          id="powerSupplyHealthCheckBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          Power Supply
                          <p
                            id="powerSupplyHealthCheck"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                      <td scope="row " className="col-md-3 ">
                        <div
                          id="hdopHealthCheckBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className=" bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          HDOP
                          <p
                            id="hdopHealthCheck"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td scope="row ">
                        <div
                          id="rssiHealthCheckBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          RSSI
                          <p
                            id="rssiHealthCheck"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                      <td scope="row ">
                        <div
                          id="cvdUptimeHealthCheckBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          CVD Uptime
                          <p
                            id="cvdUptimeHealthCheck"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                      <td scope="row ">
                        <div
                          id="vbusStatusHealthCheckBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          Vbus Status
                          <p
                            id="vbusStatusHealthCheck"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                      <td scope="row" hidden>
                        <div
                          id="fwStatusHealthCheckBox"
                          data-toggle="tooltip"
                          data-placement="top"
                          className="bg-muted p-xxs b-r-md tiny-font modal-details"
                        >
                          FW Status
                          <p
                            id="fwStatusHealthCheck"
                            className="dynamic-data font-bold"
                          >
                            n/a
                          </p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div className=" ibox-content ">
                <h4>Actions</h4>
                <div>
                  <div>
                    <select className="form-control" id="smsDropdown">
                      SMS Actions
                      <option value="!R3,70,0">Reboot Device</option>
                      <option value="!R3,99,1">Reboot Jpod</option>
                      <option value="!R3,41,225">Request Log</option>
                      <option value="!V0">Redetect Vehicle</option>
                      <option value="!R3,20,0">Reset Gps Modem </option>
                      <option value="!R3,55">Reset cell Modem </option>
                      <option value="!R3,49,129">Force Puls Check-in </option>
                    </select>
                    <button
                      type="button"
                      id="sendSms"
                      className="btn btn-primary"
                    >
                      Send SMS
                    </button>
                  </div>

                  <div className="col-md-3">
                    <a
                      className="btn btn-success "
                      id="cvdActionsPage"
                      target="_blank"
                     >
                      Go To CVD Actions
                    </a>
                  </div>
                </div>
                {/* <div style="display: inline-block;" className=""><a className="btn btn-success" id="cvdActionsPage" target="_blank">Cvd Actions Page</a></div> */}
              </div>
            </div>
          </div>
      </div>
      </>
    )
  }
}

