import React, { Component } from 'react'

export class SearchBox extends Component {
  render() {
    return (
      <div className="col-lg-12 p-sm">
        <h2>Hawkeye</h2>

        <div className="input-group float-right mail-search">
          <input
            id="entity"
            className="form-control form-control-sm "
            placeholder="Search by Entity"
            // onInput={validateInput()}
          />
          <div className="input-group-btn">
            <button id="sendQuery" className="btn btn-success ">
              <i className="fa fa-search"></i>search
            </button>
          </div>
        </div>
        <select id="searchBy" className="btn white-bg  ">
          <option value="cvd">CVD</option>
          <option value="tablet">Tablet</option>
          <option value="driver">Driver</option>
          <option value="power">Power Unit</option>
          <option value="vin">Vin</option>
        </select>
        <select id="range" className="btn white-bg  ">
          <option value="today">Today</option>
          <option value="week">This Week</option>
          <option value="month">This Month</option>
        </select>
        <div>
          <input
            id="salesforceBtn"
            disabled
            className="btn btn-primary"
            // onClick="sendToSalesforce();"
            value="Send to Salesforce"
          />
          <div id="salesforceBox" hidden>
            <input
              id="salesforceInput"
              maxLength="8"
              role="listbox"
              // autocomplete="off"
              type="text"
              placeholder="case ID"
            />
            {/* <ul
              className="dropdown-menu"
              id="autocomplete-results"
              style=" overflow: auto"
            ></ul> */}
            <input
              type="button"
              id="updateSalesForceBtn"
              className="btn btn-success "
              value="send"
              disabled
            />
          </div>
        </div>
      </div>
    )
  }
}

function validateInput() {
  // let searchKey = document.getElementById('entity')
  // let entityType = document.getElementById('searchBy').value
  // if (entityType === 'cvd' && searchKey.value.length < 10) {
  //   searchKey.style.borderColor = 'red'
  //   document.getElementById('sendQuery').disabled = true
  // } else if (entityType === 'tablet' && searchKey.value.length < 11) {
  //   searchKey.style.borderColor = 'red'
  //   document.getElementById('sendQuery').disabled = true
  // } else if (entityType === 'vin' && searchKey.value.length < 17) {
  //   searchKey.style.borderColor = 'red'
  //   document.getElementById('sendQuery').disabled = true
  // } else {
  //   document.getElementById('sendQuery').disabled = false
  //   searchKey.style.borderColor = 'black'
  // }
}
