import { Navbar } from '../../components/navbar'
import { Styles } from '../../components/styles'
import { CvdBox } from '../../components/hawkeye/cvdBox'
import { DriverBox } from '../../components/hawkeye/driverBox'
import { SearchBox } from '../../components/hawkeye/searchBox'
import { TabletBox } from '../../components/hawkeye/tabletBox'
import { VehicleBox } from '../../components/hawkeye/VehicleBox'

export default function Hawkeye() {
    return <>
    <div id="main">
      <Navbar />
      <div id="page-wrapper" className = "container">
        <div id="container" className=" row wrapper bg-light">
          <SearchBox />
          {/* <div className="col-lg-12 "> */}
            <DriverBox />
            <CvdBox />
          {/* </div> */}
          {/* <div className="col-lg-12 "> */}
            <VehicleBox />
            <TabletBox />
          {/* </div> */}
          <Styles />
        </div>
      </div>
    </div>
  </>
  }